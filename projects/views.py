from django.shortcuts import render, get_object_or_404, redirect
from projects.models import Project
from tasks.models import Task
from accounts.forms import CreateProjectForm
from django.contrib.auth.decorators import login_required


@login_required
def project_list(request):
    project = Project.objects.filter(owner=request.user)
    context = {"list_projects": project}
    return render(request, "projects/list_project.html", context)


@login_required
def project_detail(request, id):
    detail_project = get_object_or_404(Project, id=id)
    details = Task.objects.filter(project=id)
    context = {
        "show_project": detail_project,
        "details": details,
    }
    return render(request, "projects/show_project.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = CreateProjectForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect("/projects/", item)
    else:
        form = CreateProjectForm()
    context = {
        "form": form,
    }
    return render(request, "projects/create_project.html", context)
