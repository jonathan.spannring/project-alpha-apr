from django.shortcuts import render, redirect
from accounts.forms import CreateTaskForm
from django.contrib.auth.decorators import login_required
from tasks.models import Task


@login_required
def create_task(request):
    if request.method == "POST":
        form = CreateTaskForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect("/projects/", item)
    else:
        form = CreateTaskForm()
    context = {
        "form": form,
    }
    return render(request, "projects/create_task.html", context)


@login_required
def show_tasks(request):
    my_tasks = Task.objects.filter(assignee=request.user)
    context = {
        "show_my_tasks": my_tasks,
    }
    return render(request, "projects/my_tasks.html", context)
